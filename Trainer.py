import io
import json
import spacy
from snips_nlu import SnipsNLUEngine
from Services.NlpService import NlpService
from Services.NluService import NluService


# noinspection SpellCheckingInspection
def ModelTrainer():
    nlp = spacy.load("en_core_web_sm")
    with io.open("Data/data.json", 'rb') as file:
        dataset = json.load(file)

    inputdata = "Requirement The following artists are required to work on an innovative one of a kind concept of 'ACT FROM HOME':Director Choreographer Dancers Actors Poets Voice over artist Editors Interested candidates pls DM"
    nluengine = SnipsNLUEngine()
    nluengine.fit(dataset)
    nlpservice = NlpService(nlp)
    nluService = NluService(nluengine, nlpservice)
    jsonData = nluService.Parse(inputdata)
    print(json.dumps(jsonData, indent=2, sort_keys=True))


if __name__ == "__main__":
    ModelTrainer()
