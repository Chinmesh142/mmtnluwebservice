FROM python:3.8.3

# Create working directory
WORKDIR /app

COPY requirements.txt ./

RUN pip install -r requirements.txt
RUN python -m snips_nlu download en
RUN python -m spacy download en_core_web_sm

COPY Data/data.yaml /app/data.yaml
RUN snips-nlu generate-dataset en data.yaml > data.json
RUN snips-nlu train data.json nluengine

COPY Services /app/Services
COPY App.py /app/App.py

EXPOSE 80
CMD [ "python", "App.py" ]