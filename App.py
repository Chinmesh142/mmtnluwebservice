import json
import spacy
from flask import Flask, request, jsonify
from snips_nlu import SnipsNLUEngine
from Services.NlpService import NlpService
from Services.NluService import NluService

app = Flask(__name__)
nlp = spacy.load("en_core_web_sm")
nluEngine = SnipsNLUEngine.from_path("nluengine")
intent_list = ['actorintent', 'dancerintent', 'eventartistintent', 'modelintent', 'musicianintent', 'singerintent', 'stagepoetintent', 'stuntartistintent', 'voiceartistintent']

# noinspection SpellCheckingInspection
@app.route('/', methods=['POST'])
def search():
    # check is proper body is send in request expected {"query":"the value"}
    if len(request.data.strip()) == 0:
        return json.dumps({'success': False, 'Message': 'Invalid request'}), 400, {'ContentType': 'application/json'}

    data = json.loads(request.data)
    query = data.get("query", None)

    # check is proper body is send in request expected {"query":"the value"}
    if query is None:
        return json.dumps({'success': False, 'Message': 'Invalid request'}), 400, {'ContentType': 'application/json'}

    try:
        print("Call to nluParser.Parse started")
        nlpService = NlpService(nlp)
        nluService = NluService(nluEngine, nlpService)
        result = nluService.Parse(query,intent_list)
        print("Call to nluParser.Parse success")
        return json.dumps({'success': True, 'data': result}), 200, {'content-type': 'application/json'}
    except Exception as e:
        print("Call to nluParser.Parse failed" + str(e));
        return json.dumps({'success': False, 'data': 'Internal server error'}), 500, {
            'content-type': 'application/json'}
    finally:
        print("Call to nluParser.Parse completed")


if __name__ == '__main__':
    app.run(debug=False, host="0.0.0.0", port=8083)
