import json


# noinspection SpellCheckingInspection
class NluService:

    def __init__(self, nluEngine, nlpService):
        self.nlpService = nlpService
        self.nluEngine = nluEngine

    def Parse(self, input, intent_list):
        finalResult = []
        query = self.nlpService.formatQuery(input)
        for intent in intent_list:
            temp_intent_list = []
            temp_intent_list.append(intent)
            result = self.nluEngine.parse(text=query, intents=temp_intent_list)
            jsondata = json.loads(json.dumps(result))
            if jsondata["intent"]["intentName"] is not None:
                probabilityPercentage = float(jsondata["intent"]["probability"]) * 100
                if probabilityPercentage > 15:
                    jsondata['input'] = input
                    finalResult.append(jsondata)
        return json.loads(json.dumps(finalResult))
