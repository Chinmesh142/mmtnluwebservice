import re


# noinspection SpellCheckingInspection
class NlpService:

    def __init__(self, nlp):
        self.nlp = nlp

    def formatQuery(self, query):
        words = []
        separator = ' '
        cleanupquery = self.cleanUpQuery(query.strip())
        firstword = self.checkFirstWord(query.strip())
        finalquery = firstword + " " + cleanupquery
        sentence = self.nlp(finalquery.strip())
        for token in sentence:
            if token.text.strip() != "":
                words.append(token.text.strip().lower())
        return separator.join(words)

    @staticmethod
    def cleanUpQuery(query):
        result = re.sub("[`~!@#$%^&;*,{}<>?/|]", " ", query)
        return result.replace('"', " ").replace("[", " ").replace("]", " ")

    @staticmethod
    def checkFirstWord(query):
        result = ""
        firstword = query.split(" ")[0]
        firstword = firstword.strip().lower()
        if not firstword == "looking" and not firstword == "show" and not firstword == "need":
            result = "looking"
        return result
